""" Download Melanoma:
  This is the program to download the additional melanoma samples from the ISIC Archieve
  Create a folder and save the code there.
  
"""

import os
import cv2
import glob
from os.path import join
import shutil
import time
import requests
from urllib3.exceptions import ReadTimeoutError
from requests.exceptions import RequestException
import json
from PIL import Image
import imghdr


class BasicElementDownloader:
    @classmethod
    def download_img(cls, img_url, img_name, dir, max_tries=None):
        """
        Download the image from the given url and save it in the given dir,
        naming it using img_name with an jpg extension
        :param img_name:
        :param img_url: Url to download the image through
        :param dir: Directory in which to save the image
        :return Whether the image was downloaded successfully
        """
        # Sometimes their site isn't responding well, and than an error occurs,
        # So we will retry a few seconds later and repeat until it succeeds
        tries = 0
        while max_tries is None or tries <= max_tries:
            try:
                # print('Attempting to download image {0}'.format(img_name))
                response = requests.get(img_url, stream=True, timeout=20)
                # Validate the download status is ok
                response.raise_for_status()

                # Find the format of the image
                format = response.headers['Content-Type'].split('/')[1]

                # Write the image into a file
                image_string = response.raw
                img_path = join(dir, '{0}.{1}'.format(img_name, format))
                with open(img_path, 'wb') as imageFile:
                    shutil.copyfileobj(image_string, imageFile)

                # Validate the image was downloaded correctly
                cls.validate_image(img_path)

                # print('Finished Downloading image {0}'.format(img_name))
                return True
            except (RequestException, ReadTimeoutError, IOError):
                tries += 1
                time.sleep(5)

        return False

    @staticmethod
    def fetch_description(url: str) -> list:
        """

        :param id: Id of the image whose description will be downloaded
        :return: Json
        """
        # Sometimes their site isn't responding well, and than an error occurs,
        # So we will retry 10 seconds later and repeat until it succeeds
        while True:
            try:
                # Download the description
                response_desc = requests.get(url, stream=True, timeout=20)
                # Validate the download status is ok
                response_desc.raise_for_status()
                # Parse the description
                parsed_description = response_desc.json()
                return parsed_description
            except (RequestException, ReadTimeoutError):
                time.sleep(5)

    @staticmethod
    def save_description(desc, dir):
        """

        :param desc: Json
        :param dir:
        :return:
        """
        desc_path = join(dir, desc['name'])
        with open(desc_path, 'w') as descFile:
            json.dump(desc, descFile, indent=2)

    @classmethod
    def download_description(cls, url: str, dir: str) -> list:
        desc = cls.fetch_description(url)
        cls.save_description(desc, dir)

    @staticmethod
    def validate_image(image_path):
        # We would like to validate the image was fully downloaded and wasn't truncated.
        # To do so, we can open the image file using PIL.Image and try to resize it to the size
        # the file declares it has.
        # If the image wasn't fully downloaded and was truncated - an error will be raised.
        img : Image.Image = Image.open(image_path)
        img.resize(img.size)


dir_train = '../Descriptions/'  #change to your 

ans = sorted(glob.glob(dir_train + 'ISIC*'))
print(len(ans))

url_prefix: str = 'https://isic-archive.com/api/v1/image/'
url_suffix: str = '/download?contentDisposition=inline'

i = 0
ctr = 0
ctr3 = 0
while i < len(ans):
	file = open(ans[i], 'rb')
	ctr2 = 0 
	for line in file:
		if(ctr2 == 1):
			img_id = line.decode('ascii')[10:34]
		ctr2 = ctr2 +1
		if(line == b'      "diagnosis": "melanoma",\n'):
			ctr = ctr + 1
			print(ctr)
            # Please Replace with your file location
			img_scr = ans[i].replace('/home/jakesabandal/Documents/Projects/ensembled-densenet-resnet-for-melanoma-classification/Dataset/Descriptions/','')

			img_url = url_prefix + img_id + url_suffix
			BasicElementDownloader.download_img(img_url=img_url, img_name=img_scr, dir='/home/jakesabandal/Documents/Projects/ensembled-densenet-resnet-for-melanoma-classification/Dataset/')
            

	i = i +1
print(ctr)
print(ctr3)