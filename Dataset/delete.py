
# coding: utf-8

# In[1]:


#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import cv2
import glob
import csv
import pickle
import os

# In[2]:
y_train_melanoma = []
x_train_melanoma = []
counter = 0

# Training Dataset Generation
ctr = 0
with open('ISIC-2017_Test_v2_Part3_GroundTruth.csv') as csvfile:
    readCSV = csv.reader(csvfile, delimiter=',')
    sw = 0
    for row in readCSV:
        if sw == 1:
            if int(row[1][0]) == 1:

                os.system('rm -f Melanoma/'+row[0]+'.jpeg')
                print(ctr)
                ctr = ctr +1

        sw = 1