import numpy as np
import cv2
import glob
import csv
import pickle
import os
from random import shuffle

dir_src = '/home/jakesabandal/Documents/Projects/isic2017-bb/Dataset/ISIC-2017_Training_Data/'
dir_desc = '/home/jakesabandal/Documents/Projects/isic2017-bb/Dataset/Descriptions/'
ans = sorted(glob.glob(dir_src+'ISIC*.jpg'))

shuffle(ans)

print(len(ans))

with open('ISIC-2017_Training_Part3_GroundTruth_melanoma.csv', 'w', newline='') as f:
	writer = csv.writer(f,delimiter = ',')
	writer.writerow(['img_id','melanoma'])
	i=0
	while i < len(ans):
		mel_check = 0
		img_name = ans[i].replace('/home/jakesabandal/Documents/Projects/isic2017-bb/Dataset/ISIC-2017_Training_Data/','')
		img_name = img_name.replace('.jpg','')

		file = open(dir_desc + img_name, 'rb')
		for line in file:
			if(line == b'      "diagnosis": "melanoma",\n'):
				mel_check = 1
		#print(mel_check)
		#print(img_name)
		i = i + 1
		print(img_name+','+ str(mel_check))
		writer.writerow([img_name, mel_check])