import numpy as np
import cv2
import glob
import csv
import pickle
import os

#replace with file source
dir_dest = '/home/jakesabandal/Documents/Projects/ensembled-densenet-resnet-for-melanoma-classification/Dataset/ISIC-2017_Training_Data/'
dir_src = '/home/jakesabandal/Documents/Projects/ensembled-densenet-resnet-for-melanoma-classification/Dataset/Melanoma/'

ans = sorted(glob.glob(dir_src+'ISIC*.jpeg'))
print(len(ans))
i=0
while i < 1800:
	img = cv2.imread(ans[i])
	img_name = ans[i].replace('/home/jakesabandal/Documents/Projects/ensembled-densenet-resnet-for-melanoma-classification/Dataset/Melanoma/','')
	img_name = img_name.replace('jpeg','jpg')
	print(dir_dest+img_name)
	cv2.imwrite(dir_dest+img_name,img)
	i = i + 1
	print(i)