#[ISIC2017 Part 3 Challange Melanoma Detection](https://challenge.kitware.com/#challenge/583f126bcad3a51cc66c8d9a)

Skin cancer has been one of the major problems in health sector, with over 5 million newly diagnosed cases in US alone. Melanoma
is the deadliest type of skin cancer claiming atleast 9000 lives each year. In this project, combination of deep learning models were explored to increase the classification accuracy of Melanoma detection from images of skin lesions.

![Screenshot](Documentations/Melanoma.png)
---

## Installation Setup

This repository makes use of deep learning methods thus you'll need to have Python 3 with Tensorflow and Keras plus other python packages to be able to reproduce the experiment from scratch. 

1. Install [Python 3](https://www.python.org/downloads/)
2. Install [Tensorflow](https://www.tensorflow.org/) and [Keras](https://keras.io/). Note that GPU machine is needed to perform the experiments
3. Install [Anaconda](https://www.anaconda.com/). Installing this will help you install other necessary packages at ease.
4. Clone the repository.

---

## DATA
Lesion classification data includes the original image, paired with a gold standard (definitive) diagnosis, referred to as "Ground Truth".
###[Training dataset](https://challenge.kitware.com/api/v1/item/584ad09dcad3a51cc66c8e15/download)
The training dataset is composed of 2000 images of 374 Melanoma, 254 Seborrheic Keratosis, and 1372 Benign Nevus. For this part of the implementation, the focus is to perform a binary classification of each image as either Malignant(Melanoma) or Benign(Seborrheic Keratosis and Nevus).
###[Validation dataset](https://challenge.kitware.com/api/v1/item/586fe9f4cad3a51cc66c934f/download)
An optional Validation dataset was provided with 150 images.
###[Test dataset](https://challenge.kitware.com/api/v1/item/58a24018cad3a51804c44072/download)
The official test dataset is composed of 600 images with 117 Melanoma, 90 Seborrheic Keratosis 393 and Benign Nevus.

Performing binary classification with the given training and test samples would be unreliable, since all the datasets are heavily imbalanced; only around 20% of the dataset are images of diagnosed Melanoma. For example, an ineffective classifier that always yields
a Benign prediction would still achieve high accuracy on the test dataset, despite failting to identify Malignant cases. The solution is to get additional Melanoma image samples from [ISIC Archive](https://www.isic-archive.com/#!/topWithHeader/wideContentTop/main) to balance the training dataset.

The pickled datasets are already given in this repository but if you still want to replicated the procedure used please follow these steps:

1. Download [Training dataset](https://challenge.kitware.com/api/v1/item/584ad09dcad3a51cc66c8e15/download), [Validation dataset](https://challenge.kitware.com/api/v1/item/586fe9f4cad3a51cc66c934f/download), [Test dataset](https://challenge.kitware.com/api/v1/item/58a24018cad3a51804c44072/download) and extract them into the dataset folder of the repository.
2. The [Descriptions Folder](Dataset/Descriptions) contains the logs of the ISIC Archive samples, to download the external melanoma samples run `python Dataset/Melanoma/download.py`
3. Then run `python Dataset/delete.py` to avoid train-test contamination. Melanoma samples from ISIC Archive that were also an entry in the test dataset will be deleted.
4. Copy the additional melanoma samples to the training dataset, `python Dataset/copy.py`
5. Execute `python Dataset/new_groundtruth_training.py` to create the new training_csv which will be the base of the generated pickled dataset.
6. To generate the pickled data format, run `python IS17_data_generator_melanoma_colored.py` which will generate, Training, Validation, and Test pickled dataset. Note that the image pixel can be change by changing **w**, default is 64 pixel.

The new training dataset is composed of 3426 images, 1800 Melanoma and 1626 Benign(Seborrheic Keratosis and Nevus). Now we have a balanced dataset for our binary classification. Note that this new training dataset is focused for Melanoma classification.

---
## ResNET
This implementation was from the paper "[Deep Residual Learning for Image Recognition](https://arxiv.org/pdf/1512.03385.pdf)"
Run `python ResNET_melanoma.py` to predict or evaluate the test dataset. Uncomment Data Augmentation part to re-train the model.

Several attempts of ResNET were tested on a 200 epoch 64x64 pixel dataset. Here are the sample results of those attempts and their different iterations.

Depth         | Time/epoch	  | Parameters | Test Accuracy
------------- | ------------- | -----------| -------------
20(n=7) 	  | 33s 		  | 1,948,130  | 0.81667
155(n=17) 	  | 74s  		  | 4,697,250  | 0.82000
245(n=27) 	  | 116s  		  | 7,446,370  | 0.81167
335(n=37) 	  | 158s  		  | 10,195,490 | 0.84000

Cropping and Censorship methods were also tested. Censorship is when you use online data augmentation and cover a portion of the image by black pixel blocks, though none posed significant improvement and contributed to much slower training.
Training the image in grayscale also produce lower accuracy compared to a full 3 channel dataset. Saved_weights_only was activated in the Model checkpoints since saving the whole model is too large for my machine to handle.

![Screenshot](Documentations/ResNET-335.png)

---
## DenseNET
DenseNET implementation was from the paper "[DenResNet: Ensembling Dense Networks and Residual Networks](https://arxiv.org/pdf/1608.06993.pdf)"
Run `python DenseNET_melanoma.py` to predict or evaluate the test dataset. Uncomment Data Augmentation part to retrain the model.

Experiments using different pixel size and depth value was evaluated with the DenseNET model.

Pixel |Depth         | Time/epoch	  | Parameters | Test Accuracy
------|------------- | -------------  | -----------| -------------
w=32  |200 	  		 | 120s 		  | 2,587,220  | 0.82667
w=64  |100 	  		 | 82s  		  | 797,096    | 0.84000
w=64  |200 	  		 | 299s  		  | 2,591,288  | 0.85500
w=128 |NA 	  		 | Too large  	  | NA 		   | NA

Some of the test were repeated more than once and the highest result were the once recorded.

![Screenshot](Documentations/DenseNET-200.png)

---
## Ensembled Model
This model was the combined results of highest ResNET and DenseNET which resulted to a result of 87% Test Accuracy, the highest ever recorded in all the test experiments. [Existing implementation](http://cs231n.stanford.edu/reports/2017/pdfs/933.pdf) proves that such ensemble improves accuracy.

![Screenshot](Documentations/Ensemble.png)

The last layer of each model was supposedly a *Dense layer* which outputs the binary classification, but for us to do a more meaningful ensemble, we combine the outputs of the the "*flatten layer*", which comes immediately before the final *dense layer* in each of our two models.
We take the output from *flatten layer* of ResNET and the output from the *flatten layer* of DenseNET, apply *dense layer* of 256 *hidden units* with *softmax activation* to each and concatenate them into one tensor, and input this concatenation to one final *dense layer* .
The final *dense layer* will then output the ensemble's overall binary classification: [Melanoma, 1-Melanoma]

```
	inputs_ResNET = Input(shape=(1,1024))
	inputs_DenseNET = Input(shape=(1,2712))
	y_ResNET = Flatten()(inputs_ResNET)
	y_ResNET = Dense(hidden_units,
				  kernel_initializer='he_normal',
				  activation='softmax')(y_ResNET)
	y_DenseNET = Flatten()(inputs_DenseNET)
	y_DenseNET = Dense(hidden_units,
				  kernel_initializer='he_normal',
				  activation='softmax')(y_DenseNET)

	y = concatenate([y_ResNET, y_DenseNET])
	outputs3 = Dense(num_classes,
				  kernel_initializer='he_normal',
				  activation='softmax')(y)
	model_Ensemble = Model(inputs=[inputs_ResNET,inputs_DenseNET], outputs=outputs3)
```

![Screenshot](Documentations/Ensembled_ResNET_DenseNET.png)

---
## Conclusion
The final Test Accuracy achieved was 87% which is close to the top scorer of the challenge which was 87.4%. The highest score was not beaten even though the validation was already set as the Test data which means that it must record the highest test result and not just the validation.
The discrepancy might be from the dataset that was used by the top scorer, because in their paper they said they used the full ISIC Archive as their training dataset without excluding the duplication in the test dataset, that might have caused an train-test contamination given that they achieve high score even though their data set is imbalanced.

Some of the codes were derived from @roatienza implementation of [DenseNET](https://github.com/PacktPublishing/Advanced-Deep-Learning-with-Keras/blob/master/chapter2-deep-networks/densenet-cifar10-2.4.1.py) and [ResNET](https://github.com/keras-team/keras/blob/master/examples/cifar10_resnet.py) in github. 